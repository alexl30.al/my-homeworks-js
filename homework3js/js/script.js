"use strict";
let x = "";
let y = "";
let operation = "";

x = prompt("Enter number X", x);

while (isNaN(x) || x.trim() === "") {
  x = prompt("Enter number X", x);
}

y = prompt("Enter number Y", y);

while (isNaN(y) || y.trim() === "") {
  y = prompt("Enter number Y", y);
}
operation = prompt("Enter a sign of operation: +, -, *, /", operation);

while (
  operation !== "+" &&
  operation !== "-" &&
  operation !== "*" &&
  operation !== "/"
) {
  operation = prompt("Enter a sign of operation: +, -, *, /", operation);
}

function calcXY(x, y, operation) {
  switch (operation) {
    case "+":
      return +x + +y;
    case "-":
      return x - y;
    case "*":
      return x * y;
    case "/":
      return x / y;
  }
}
console.log(calcXY(x, y, operation));
