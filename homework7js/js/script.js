"use strict";

const incomingList = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let parentElm = document.createElement("ul");
function createList(incomingList) {
  const arrItems = incomingList.map((el) => {
    let item = document.createElement("li");
    item.textContent = el;
    parentElm.appendChild(item);
    return item;
  });

  console.log(arrItems);
  console.log(...arrItems);
}
createList(incomingList);
document.body.prepend(parentElm);
