"use strict";

let name = prompt("Enter your Name", "Евлампий");
while (!isNaN(name)) {
  name = prompt("Enter your Name", "Евлампий");
}

let age = prompt("Enter your age", "20");
while (age === null || isNaN(+age) || age.trim() === "") {
  age = prompt("Enter your age", "20");
}

if (age < 18) {
  alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
  age = confirm("Are you sure you want to continue");
  if (age) {
    alert(`Welcome,  ${name}`);
  } else {
    alert("You are not allowed to visit this website.");
  }
} else if (age > 22) {
  alert(`Welcome,  ${name}`);
}
