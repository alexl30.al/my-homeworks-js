"use strict";

const tabTitle = document.querySelectorAll(".tabs-title ");
const lengthTabTitile = tabTitle.length;
const tabSwitcher = () => {
  return function () {
    let tabContent = document.querySelector(
      `.tabs-content[data-content="${this.dataset.content}"]`
    );
    document.querySelector(".tabs-title.active").classList.remove("active");
    document.querySelector(".tabs-content.active").classList.remove("active");
    tabContent.classList.add("active");
    this.classList.add("active");
  };
};

for (let i = 0; i < lengthTabTitile; i++) {
  tabTitle[i].addEventListener("click", tabSwitcher());
}
