"use strict";

function createNewUser() {
  let fN = prompt("Enter you first name: ", "");
  while (!isNaN(fN)) {
    fN = prompt("Enter you first name: ", "");
  }
  let lN = prompt("Enter you last name", "");
  while (!isNaN(lN)) {
    lN = prompt("Enter you first name: ", "");
  }
  let bDay = prompt("Enter a date of your birthday", "dd.mm.yyyy");

  return {
    firstName: fN,
    lastName: lN,
    birthDay: bDay.split(".").reverse().join("."),

    getAge: function () {
      const date1 = new Date(this.birthDay);
      const date2 = new Date(Date.now());
      let age;
      age = (date2 - date1) / (1000 * 60 * 60 * 24 * 365);
      return Math.trunc(age);
    },
    getPassword: function () {
      let newPasss =
        this.firstName.charAt(0).toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthDay.substr(0, 4);
      return newPasss;
    },

    getLogin: function () {
      let newLogin =
        this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
      return newLogin;
    },
  };
}

let newUser = createNewUser();
console.log(`Your login is: ${newUser.getLogin()}`);
console.log(`Your Age is: ${newUser.getAge()}`);
console.log(`Your Password is: ${newUser.getPassword()}`);
