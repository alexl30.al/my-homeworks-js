"use strict";

let typeOfData = prompt(
  "Enter a type of data",
  "undefined,null,number,bigint,string,boolean"
);

let items = ["hello", "world", undefined, 23, "23", null, false, true, 1n, 0];

const filterBy = (arr, typeOfData) =>
  arr.filter((item) => typeof item !== typeOfData);

console.log(items);
console.log(filterBy(items, typeOfData));
