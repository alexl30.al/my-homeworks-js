"use strict";

let buttons = document.querySelectorAll(".btn"),
  activeButton;

if (buttons) {
  buttons = [...buttons];
}

function btnPress(e) {
  activeButton = document.querySelector(".active");
  if (activeButton) {
    activeButton.classList.remove("active");
  }

  buttons.map((btn) => {
    if (e.key === btn.innerText.toLowerCase() || e.code === btn.innerText) {
      btn.classList.add("active");
    }
  });
}

document.body.addEventListener("keypress", btnPress);
