"use strict";

const eye1 = document.getElementById("open-eye1");
const input1 = document.getElementById("password-input1");
function switchEye1() {
  if (
    eye1.classList.contains("fa-eye") &&
    input1.getAttribute("type") == "password"
  ) {
    eye1.classList.replace("fa-eye", "fa-eye-slash");
    input1.setAttribute("type", "text");
  } else if (eye1.classList.contains("fa-eye-slash")) {
    eye1.classList.replace("fa-eye-slash", "fa-eye");
    input1.setAttribute("type", "password");
  }
}

eye1.addEventListener("click", switchEye1);

const eye2 = document.getElementById("open-eye2");
const input2 = document.getElementById("password-input2");
function switchEye2() {
  if (
    eye2.classList.contains("fa-eye") &&
    input2.getAttribute("type") == "password"
  ) {
    eye2.classList.replace("fa-eye", "fa-eye-slash");
    input2.setAttribute("type", "text");
  } else if (eye2.classList.contains("fa-eye-slash")) {
    eye2.classList.replace("fa-eye-slash", "fa-eye");
    input2.setAttribute("type", "password");
  }
}
eye2.addEventListener("click", switchEye2);

const buttonSbm = document.querySelector(".password-form");
const btn = document.querySelector(".btn");
const warning = document.querySelector(".warning");
function compare(event) {
  if (input1.value == "") {
    warning.innerText = "Enter the password";
  } else if (input1.value !== input2.value) {
    warning.innerText = "Нужно ввести одинаковые значения";
  } else {
    event.preventDefault();
    warning.innerText = "";
    input1.value = "";
    input2.value = "";
    setTimeout(() => alert("You are welcome"), 100);
  }
}

buttonSbm.addEventListener("submit", compare);
