"use strict";
const cunsumer = document.querySelector(".cunsumer-input");
const meaning = document.querySelector(".instant-value");
const price = document.querySelector(".price");
const input = document.querySelector(".userp");
const warning = document.createElement("span");
const form = document.querySelector(".form");
const closed = document.querySelector(".close");

function changeColor1() {
  input.style.border = "6px solid green";
  input.style.outline = "none";
}
function changeColor2() {
  price.innerText = `Curent Price : ${input.value}`;
  if (!isNaN(input.value) && input.value > 0) {
    input.style.background = "green";
    price.style.background = "green";
    meaning.style.visibility = "visible";
    form.removeChild(warning);
  } else if (input.value == "") {
    input.style.border = "6px solid #FFFFFF";
    input.style.background = "none";
    form.removeChild(warning);
  } else {
    input.style.border = "6px solid red";
    input.style.background = "none";
    warning.style.color = "red";
    warning.innerText = `Please enter correct price!`;
    warning.style.textAlign = "center";
    warning.style.marginTop = "10px";
    meaning.style.visibility = "hidden";
    form.appendChild(warning);
  }
}

function closeCurPr() {
  input.value = "";
  meaning.style.visibility = "hidden";
  input.style.background = "none";
  input.style.border = "6px solid #FFFFFF";
}

input.addEventListener("focus", changeColor1);
input.addEventListener("blur", changeColor2);
closed.addEventListener("click", closeCurPr);
