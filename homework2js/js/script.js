"use strict";

let maxNum = prompt("Enter number", "");
while (maxNum % 1 !== 0);
if (maxNum < 0) {
  console.log("Sorry, no numbers");
}

for (let i = 0; i <= maxNum; i++) {
  if (maxNum < 5) {
    console.log("Sorry, no numbers");
    break;
  } else if (i % 5 === 0) {
    console.log(i);
  }
}
// let m;
// let n;
// do {
//   m = +prompt("Enter number m", "");
//   n = +prompt("Enter number n", "");
//   if (m < 2 || m > n || m % 1 !== 0 || n % 1 !== 0) {
//     alert("Sorry, no numbers");
//   }
// } while (m < 2 || m > n || m % 1 !== 0 || n % 1 !== 0);

// nextNumber: for (let i = m; i <= n; ++i) {
//   for (let j = 2; j < i; ++j) {
//     if (i % j === 0) continue nextNumber;
//   }

//   console.log(i);
// }
