"use strict";
let numF0 = prompt("Enter the first number of Fibonachi sequence:", "");

while (isNaN(numF0) || numF0 === null || numF0.trim() === "") {
  numF0 = prompt("Enter the first number of Fibonachi sequence:", "");
}

let numF1 = prompt("Enter the second number of Fibonachi sequence:", "");
while (isNaN(numF1) || numF1 === null || numF1.trim() === "") {
  numF1 = prompt("Enter the second number of Fibonachi sequence:", "");
}
let n = prompt("Enter the  number of element of Fibonachi sequence:", "");
while (isNaN(n) || n === null || n.trim() === "" || n < 0) {
  n = prompt("Enter the  number of element of Fibonachi sequence:", "");
}

function fibonachi(numF0, numF1, n) {
  let summ = 0;

  if (n <= 1) {
    return n;
  } else if (n % 2 !== 0 && numF1 < 0) {
    for (let i = 2; i <= n; i++) {
      summ = +numF0 + +numF1;
      numF0 = numF1;
      numF1 = summ;
    }
    return -summ;
  } else {
    for (let i = 2; i <= n; i++) {
      summ = +numF0 + +numF1;
      numF0 = numF1;
      numF1 = summ;
    }
    return summ;
  }
}
console.log(fibonachi(numF0, numF1, n));
